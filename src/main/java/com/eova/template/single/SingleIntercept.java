/**
 * Copyright (c) 2013-2016, Jieven. All rights reserved.
 *
 * Licensed under the GPL license: http://www.gnu.org/licenses/gpl.txt
 * To use it on other terms please contact us at 1623736450@qq.com
 */
package com.eova.template.single;

import java.util.Map;

import com.eova.aop.AopContext;
import com.eova.common.utils.excel.ExcelUtil;

/**
 * 单表模版业务拦截器<br>
 * 前置任务和后置任务(事务未提交)<br>
 * 成功之后(事务已提交)<br>
 * 
 * @author Jieven
 * @date 2014-8-29
 */
public class SingleIntercept {
	
	/**
	 * 
	 * 导入前置任务(事务内)
	 * 主要是设置excel一些参数（前2个参数运行前段传递，本处覆盖）：
	 * sheet =	ctrl.getAttrForInt(ExcelUtil.IMP_EXCEL_DEFAULT_SHEET);
	 * fisrtline =	ctrl.getAttrForInt(ExcelUtil.IMP_EXCEL_SHEET_FIRSTLINE);
	 * enColumn =	(Map)ctrl.getAttr(ExcelUtil.IMP_EXCEL_COLUMN2EN);
	 */
	public void importBefore(AopContext ac) throws Exception {
	}
	
	/**
	 * excel初始数据后前置任务(事务内)
	 * ac.ctrl.getAttr(ExcelUtil.IMP_EXCEL_SHEET); excel数据如果需要再次处理的话
	 * @param ac 
	 * @throws Exception
	 */
	public void importInit(AopContext ac) throws Exception {
		
	}
	

	/**
	 * 导入后置任务(事务内)
	 * 
	 */
	public void importAfter(AopContext ac) throws Exception {
	}

	/**
	 * 导入成功之后(事务外)
	 * 
	 */
	public void importSucceed(AopContext ac) throws Exception {
	}
}